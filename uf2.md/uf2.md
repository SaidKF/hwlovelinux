## 10 novembre 2020

##lsblk
- La comanda lsblk serveix per a extraure informacio de les particions, sistemes de fitxers i punts de muntatge:

[guest@a06 ~]$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0  17.9M  1 loop /var/lib/snapd/snap/pdftk/9
loop1    7:1    0  97.1M  1 loop /var/lib/snapd/snap/core/9993
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]

##-O, --output-all     output all columns
- Ens dona tota la informacio del disc.

WSAME WWN                RAND PKNAME HCTL       TRAN   SUBSYSTEMS  REV VENDOR ZONED
loop0
     loop0 /dev/loop0
                  7:0         0  17.9M squash  17.9M   100% 4.0   /var/lib/s                                                                                                                                                                       128  1  0       0               17.9M       root  disk  brw-rw----         0    512      0     512     512    1 mq-deadline
                                                                                                                                                                                                                                                                                                                                                                             256 loop        0        4K       4G         0    0B                       0                          block                  none
loop1
     loop1 /dev/loop1
                  7:1         0  97.1M squash  97.1M   100% 4.0   /var/lib/s                                                                                                                                                                       128  1  0       0               97.1M       root  disk  brw-rw----         0    512      0     512     512    1 mq-deadline


##lsblk -o NAME,MODEL
- Aquesta comanda et dona la informacio que buscas posant al costat de la comanda el que vols trobar.
NAME   MODEL
loop0  
loop1  
sda    ST500DM002-1BD142
├─sda1 
├─sda5 
├─sda6 
└─sda7 

##Alias
- Serveix per a crear comandes per a que mes envdevant sigui mes facil posarla i que la recordis.
[root@a06 ~]# alias lsdisk='lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT'
[root@a06 ~]# lsdisk
NAME   MODEL             FSTYPE     SIZE TYPE MOUNTPOINT
loop0                    squashfs  17.9M loop /var/lib/snapd/snap/pdftk/9
loop1                    squashfs  97.1M loop /var/lib/snapd/snap/core/9993
sda    ST500DM002-1BD142          465.8G disk 
├─sda1                                1K part 
├─sda5                   ext4       100G part /
├─sda6                   ext4       100G part 
└─sda7                   swap         5G part [SWAP]

##SMART
- Monitoriza i reporta informacio sobre el disc dur, hi ha una comanda que es el smertctl -a.
================================================== SMARTCTL EXAMPLES =====

  smartctl --all /dev/sda                    (Prints all SMART information)

  smartctl --smart=on --offlineauto=on --saveauto=on /dev/sda
                                              (Enables SMART on first disk)

  smartctl --test=long /dev/sda          (Executes extended disk self-test)

  smartctl --attributes --log=selftest --quietmode=errorsonly /dev/sda
                                      (Prints Self-Test & Attribute errors)
  smartctl --all --device=3ware,2 /dev/sda
  smartctl --all --device=3ware,2 /dev/twe0
  smartctl --all --device=3ware,2 /dev/twa0
  smartctl --all --device=3ware,2 /dev/twl0
          (Prints all SMART info for 3rd ATA disk on 3ware RAID controller)
  smartctl --all --device=hpt,1/1/3 /dev/sda
          (Prints all SMART info for the SATA disk attached to the 3rd PMPort
           of the 1st channel on the 1st HighPoint RAID controller)
  smartctl --all --device=areca,3/1 /dev/sg2
          (Prints all SMART info for 3rd ATA disk of the 1st enclosure
           on Areca RAID controller)


##Hi ha 2 tipus de tecnologia:

- Disc Rotacional
El el disc dur habitual que te plats magnetics i un braç de lectura 

- Disc SSD 
Son chips que alacenen arxius en microchips amb memoria flash interconectades entre si.


##df -h
- Et dona especificacions del disc com capacitat,memoria utilizada.

##dd if=/dev/urandom of=/mnt/prova2G bs=2048 count=1048576
- Aquesta comanda crea un fitxer.
